-module(ttt_setup).
-export([build_initial_game_state/0]).

-ifdef(TEST).
-export([build_turn/0,
         build_opponent/0,
         build_players/0]).
-endif.

-define(BOARD_SIZE, 3).
-include("src/macros/players.hrl").
-include("src/macros/console_ttt.hrl").

build_initial_game_state() ->
  ?GAME_STATE:update_multiple(dict:new(),
                              [players, game_board, is_running],
                              [build_players(), ?BOARD:new_board(?BOARD_SIZE), true]).

build_turn() ->
  TurnInput = ?UI:prompt_turn(),
  build_turn(TurnInput).
build_turn("1") ->
  ?X_PIECE;
build_turn("2") ->
  ?O_PIECE;
build_turn(_TurnInput) ->
  ?UI:print_invalid_input_error(),
  build_turn().

build_opponent() ->
  OpponentInput = ?UI:prompt_human_or_ai(),
  build_opponent(OpponentInput).
build_opponent("1") ->
  ?COMPUTER_NAME;
build_opponent("2") ->
  ?PLAYER_2_NAME;
build_opponent(_OpponentInput) ->
  ?UI:print_invalid_input_error(),
  build_opponent().

build_players() ->
  TurnPiece = build_turn(),
  Opponent = build_opponent(),
  build_players_helper(TurnPiece, Opponent).

build_players_helper(?O_PIECE, ?COMPUTER_NAME) ->
  [[hard_ai,?X_PIECE, ?COMPUTER_NAME], [human_player, ?O_PIECE, ?PLAYER_1_NAME]];
build_players_helper(?X_PIECE, ?COMPUTER_NAME) ->
  [[human_player, ?X_PIECE, ?PLAYER_1_NAME], [hard_ai, ?O_PIECE, ?COMPUTER_NAME]];
build_players_helper(?X_PIECE, ?PLAYER_2_NAME) ->
  [[human_player, ?X_PIECE, ?PLAYER_1_NAME], [human_player, ?O_PIECE, ?PLAYER_2_NAME]];
build_players_helper(?O_PIECE, ?PLAYER_2_NAME) ->
  [[human_player, ?X_PIECE, ?PLAYER_2_NAME], [human_player, ?O_PIECE, ?PLAYER_1_NAME]].
