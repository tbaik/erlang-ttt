-module(ttt_ui).
-export([print_board/1,
         print_invalid_input_error/0,
         print_invalid_move_error/0,
         print_placed_piece/3,
         print_tie_game/0,
         print_winner/1,
         prompt_turn/0,
         prompt_play_again/0,
         prompt_human_or_ai/0,
         prompt_new_move/1]).
-include("src/macros/console_ttt.hrl").

print_board(Board) ->
  ?IO:print_message(board_presenter:board_string(Board)).

print_invalid_input_error() ->
  ?IO:print_message("Invalid Input! Please try again.\n").

print_invalid_move_error() ->
  ?IO:print_message("Invalid Move! Please try again.\n").

print_placed_piece(Spot, Piece, PlayerName) ->
  ?IO:print_message(PlayerName ++ " placed " ++ Piece ++ " at " ++ Spot ++ ".\n").

print_tie_game() ->
  ?IO:print_message("It's a tie!\n").

print_winner(Name) ->
  ?IO:print_message(Name ++ " won!\n").

prompt_turn() ->
  ?IO:prompt("Type 1 to go First(X), 2 to go Second(O):\n").

prompt_play_again() ->
  ?IO:prompt("Type 1 to Play Again, 2 to Exit:\n").

prompt_human_or_ai() ->
  ?IO:prompt("Please type 1 to play against a Computer Player or 2 to play against another Person:\n").

prompt_new_move(PlayerName) ->
  ?IO:prompt("Here is the Game Board, " ++ PlayerName ++ ". Please enter an empty space number:\n").
