-module(console_io).
-export([print_message/1,
         prompt/1]).

print_message(Message) ->
  io:format(Message).

prompt(Message) ->
  string:strip(io:get_line(Message), right, $\n).
