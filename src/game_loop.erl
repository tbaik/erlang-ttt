-module(game_loop).
-export([run/2]).
-include("src/macros/console_ttt.hrl").

run(GameModule, GameState) ->
  IsRunning = ?GAME_STATE:get_is_running(GameState),
  run(GameModule, GameState, IsRunning).
run(GameModule, GameState, true) ->
  Input = GameModule:get_input(GameState),
  NewGameState = GameModule:update(GameState, Input),
  GameModule:draw(NewGameState),
  run(GameModule, NewGameState);
run(_GameModule, _GameState, false) ->
  finished_game.
