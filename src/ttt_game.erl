-module(ttt_game).
-export([get_input/1,
         update/2,
         draw/1]).
-include("src/macros/console_ttt.hrl").

get_input(GameState) ->
  CurrentPlayer = ?PLAYERS:current_player(?GAME_STATE:get_players(GameState)),
  PlayerModule = ?PLAYERS:module(CurrentPlayer),
  PlayerModule:choose_move(GameState).

update(GameState, Input) ->
  Board = ?GAME_STATE:get_board(GameState),
  update_when_valid(GameState, Input, Board, ?RULE:is_valid_move(Board, Input)).

update_when_valid(GameState, Input, Board, true) ->
  Players = ?GAME_STATE:get_players(GameState),
  UpdatedBoard = ?BOARD:place_piece(?PLAYERS:current_player_piece(Players), Input, Board),
  UpdatedPlayers = ?PLAYERS:change_turn(Players),
  IsGameOver = ?RULE:game_over(UpdatedBoard),

  ?GAME_STATE:update_multiple(GameState,
                              [game_board, players, update_status, last_move, is_running],
                              [UpdatedBoard, UpdatedPlayers, current_status(IsGameOver), Input, not IsGameOver]);
update_when_valid(GameState, _Input, _Board, false) ->
  ?GAME_STATE:update(GameState, update_status, invalid_move).

draw(GameState) ->
  draw(GameState, ?GAME_STATE:get_update_status(GameState)).
draw(GameState, made_move) ->
  PreviousPlayer = ?PLAYERS:previous_player(?GAME_STATE:get_players(GameState)),

  ?UI:print_board(?GAME_STATE:get_board(GameState)),
  ?UI:print_placed_piece(?GAME_STATE:get_last_move(GameState),
                         ?PLAYERS:piece(PreviousPlayer),
                         ?PLAYERS:name(PreviousPlayer));
draw(GameState, invalid_move) ->
  ?UI:print_board(?GAME_STATE:get_board(GameState)),
  ?UI:print_invalid_move_error();
draw(GameState, game_over) ->
  draw(GameState, made_move),
  Winner = ?RULE:determine_winner(GameState),
  draw_winner(Winner).

draw_winner(tie) ->
  ?UI:print_tie_game();
draw_winner(Winner) ->
  ?UI:print_winner(Winner).


current_status(true) ->
  game_over;
current_status(false) ->
  made_move.
