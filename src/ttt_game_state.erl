-module(ttt_game_state).
-export([get_players/1,
        get_board/1,
        get_update_status/1,
        get_last_move/1,
        get_is_running/1,
        update/3,
        update_multiple/3]).

get_players(GameState) ->
  dict:fetch(players, GameState).

get_board(GameState) ->
  dict:fetch(game_board, GameState).

get_update_status(GameState) ->
  dict:fetch(update_status, GameState).

get_last_move(GameState) ->
  dict:fetch(last_move, GameState).

get_is_running(GameState) ->
  dict:fetch(is_running, GameState).

update(GameState, Key, Value) ->
  dict:store(Key, Value, GameState).

update_multiple(GameState, Keys, _Values) when length(Keys) =:= 0 ->
  GameState;
update_multiple(GameState, Keys, Values) when length(Keys) > 0 ->
  [Key|RestKeys] = Keys,
  [Value|RestValues] = Values,
  NewGameState = update(GameState, Key, Value),
  update_multiple(NewGameState, RestKeys, RestValues).
