-module(hard_ai).
-export([choose_move/1]).

-ifdef(TEST).
-export([minimax/2,
         get_scores/2]).
-endif.
-include("src/macros/console_ttt.hrl").

choose_move(GameState) ->
  Players = ?GAME_STATE:get_players(GameState),
  Board = ?GAME_STATE:get_board(GameState),
  ValidMoves = ?BOARD:valid_moves(Board),
  Scores = get_scores(Players, Board),
  IndexOfMaxScore = string:str(Scores, [lists:max(Scores)]),

  integer_to_list(lists:nth(IndexOfMaxScore, ValidMoves)).

minimax(Players, Board) ->
  MemoizedResult = get({'minimax', Players, Board}),
  memoized_minimax(Players, Board, MemoizedResult).
memoized_minimax(Players, Board, 'undefined') ->
  HasWinner = ?RULE:has_winner(Board),
  BoardIsFull = ?BOARD:is_full(Board),
  IsComputerTurn = ?PLAYERS:is_computer_turn(Players),

  Result = compute_minimax(Players, Board, HasWinner, BoardIsFull, IsComputerTurn),
  put({'minimax', Players, Board}, Result),
  Result;
memoized_minimax(_Players, _Board, Result) when is_integer(Result) ->
  Result.

compute_minimax(Players, Board, false, false, true) ->
  Scores = get_scores(Players, Board),
  lists:max(Scores);
compute_minimax(Players, Board, false, false, false) ->
  Scores = get_scores(Players, Board),
  lists:min(Scores);
compute_minimax(_Players, _Board, HasWinner, _BoardIsFull, IsComputerTurn) ->
  game_evaluation(HasWinner, IsComputerTurn).

game_evaluation(false, _IsComputerTurn) -> 1;
game_evaluation(true, true) -> 0;
game_evaluation(true, false) -> 2.

get_scores(Players, Board) ->
  CurrentPlayerPiece = ?PLAYERS:current_player_piece(Players),
  ChangedPlayers = ?PLAYERS:change_turn(Players),
  lists:map(fun(Move) ->
                minimax(ChangedPlayers,
                        ?BOARD:place_piece(CurrentPlayerPiece, Move, Board)) end,
            ?BOARD:valid_moves(Board)).
