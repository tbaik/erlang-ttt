-module(players).
-export([name/1,
         module/1,
         piece/1,
         current_player/1,
         current_player_piece/1,
         previous_player/1,
         change_turn/1,
         is_computer_turn/1]).

name(Player) ->
  lists:last(Player).

module(Player) ->
  hd(Player).

piece(Player) ->
  lists:nth(2, Player).

current_player(Players) ->
  hd(Players).

current_player_piece(Players) ->
  piece(current_player(Players)).

previous_player(Players) ->
  lists:last(Players).

change_turn(Players) ->
  lists:reverse(Players).

is_computer_turn(Players) ->
  module(current_player(Players)) =:= hard_ai.
