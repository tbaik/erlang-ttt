-module(human_player).
-export([choose_move/1]).
-include("src/macros/console_ttt.hrl").

choose_move(GameState) ->
  Players = ?GAME_STATE:get_players(GameState),
  Board = ?GAME_STATE:get_board(GameState),
  ?UI:print_board(Board),
  ?UI:prompt_new_move(players:name(players:current_player(Players))).
