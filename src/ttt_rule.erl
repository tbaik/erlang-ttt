-module(ttt_rule).
-export([is_valid_move/2,
         game_over/1,
         has_winner/1,
         determine_winner/1]).

-ifdef(TEST).
-export([contains_same_pieces/1,
         any_true/1,
         has_winner_from_separation/1,
         has_horizontal_winner/2,
         has_vertical_winner/2,
         has_diagonal_winner/2]).
-endif.
-include("src/macros/console_ttt.hrl").

is_valid_move(Board, Move) ->
  try
    MoveAsInt = list_to_integer(Move),
    lists:nth(MoveAsInt, Board) =:= MoveAsInt
  catch
    error:badarg ->
      false;
    error:function_clause ->
      false
  end.

determine_winner(GameState) ->
  Board = ?GAME_STATE:get_board(GameState),
  determine_winner(GameState, has_winner(Board)).
determine_winner(GameState, true) ->
  Players = ?GAME_STATE:get_players(GameState),
  ?PLAYERS:name(?PLAYERS:previous_player(Players));
determine_winner(_GameState, false) ->
  tie.

game_over(Board) ->
  board:is_full(Board) or has_winner(Board).

has_winner(Board) ->
  Width = ?BOARD:width(Board),
  any_true([has_horizontal_winner(Board, Width),
            has_vertical_winner(Board, Width),
            has_diagonal_winner(Board, Width)]).

any_true(List) ->
  lists:any(fun(Element) -> Element =:= true end, List).

has_winner_from_separation(List) ->
  any_true(lists:map(fun contains_same_pieces/1, List)).

contains_same_pieces(List) ->
  lists:all(fun(Element) -> hd(List) =:= Element end, List).

has_horizontal_winner(Board, Width) ->
  BoardAsMatrix = matrix:create_matrix(Board, Width),
  has_winner_from_separation(BoardAsMatrix).

has_vertical_winner(Board, Width) ->
  has_winner_from_separation(matrix:separate_into_columns(Board, Width)).

has_diagonal_winner(Board, Width) ->
  has_winner_from_separation(matrix:separate_into_diagonals(Board, Width)).
