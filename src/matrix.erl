-module(matrix).
-export([create_matrix/2,
         separate_into_columns/2,
         separate_into_diagonals/2]).

create_matrix([], _) -> [];
create_matrix(List, Width) ->
  try lists:split(Width, List) of
    {H,T} -> [H|create_matrix(T, Width)]
  catch
    error:badarg -> [List]
  end.

separate_into_columns(List, Width) ->
  Matrix = create_matrix(List, Width),
  transpose(Matrix).

separate_into_diagonals(List, Width) ->
  Matrix = create_matrix(List, Width),
  [get_left_diagonal(Matrix), get_right_diagonal(Matrix)].

transpose([[]|_]) -> [];
transpose(Matrix) ->
  Heads = lists:map(fun hd/1, Matrix),
  Tails = lists:map(fun tl/1, Matrix),
  [Heads|transpose(Tails)].

get_left_diagonal([]) -> [];
get_left_diagonal(Matrix) ->
  Head = hd(hd(Matrix)),
  TailOfTails = lists:map(fun tl/1, tl(Matrix)),
  [Head|get_left_diagonal(TailOfTails)].

get_right_diagonal(Matrix) ->
  lists:reverse(get_left_diagonal(lists:reverse(Matrix))).

