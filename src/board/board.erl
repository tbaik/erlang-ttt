-module(board).
-export([new_board/1,
        place_piece/3,
        width/1,
        valid_moves/1,
        is_full/1]).

new_board(Size) when is_integer(Size), Size > 0 ->
  lists:seq(1, Size*Size);
new_board(_) ->
  [].

place_piece(Piece, Spot, Board) when is_integer(Spot) ->
  tuple_to_list(setelement(Spot, list_to_tuple(Board), Piece));
place_piece(Piece, Spot, Board) ->
  tuple_to_list(setelement(list_to_integer(Spot), list_to_tuple(Board), Piece)).

width(Board) ->
  trunc(math:sqrt(length(Board))).

valid_moves(Board) ->
  lists:filter(fun is_integer/1, Board).

is_full(Board) ->
  length(valid_moves(Board)) =:= 0.
