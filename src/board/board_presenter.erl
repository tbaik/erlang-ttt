-module(board_presenter).
-export([board_string/1]).

-ifdef(TEST).
-export([blank_line_separator/1,
        row_separator/1,
        value_row_string/1,
        value_block_string/2]).
-endif.

-include("src/macros/board_presenter.hrl").

board_string(Board) ->
  Width = board:width(Board),
  lists:foldl(fun(RowValueList, ResultString) ->
                  ResultString ++ value_block_string(Width, RowValueList) end,
              "",
              matrix:create_matrix(Board, Width)).

blank_line_separator(Width) ->
  string:copies(?BLANK_LINE_SEPARATOR, Width) ++ ?NEW_LINE.

row_separator(Width) ->
  string:copies(?ROW_SEPARATOR, Width) ++ ?NEW_LINE.

value_row_string(RowValueList) ->
  lists:foldl(fun(Value, ResultString) ->
    ResultString ++ value_string(Value) end, "", RowValueList) ++ ?NEW_LINE.

value_string(Value) when is_integer(Value) ->
  value_string(integer_to_list(Value));
value_string(Value) when is_list(Value), length(Value) =:= 2 ->
  " " ++ Value ++ "  |";
value_string(Value) when is_list(Value), length(Value) =:= 1 ->
  "  " ++ Value ++ "  |".

value_block_string(Width, RowValueList) ->
  blank_line_separator(Width) ++
  value_row_string(RowValueList) ++
  blank_line_separator(Width) ++
  row_separator(Width).
