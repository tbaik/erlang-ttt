-module(board_presenter_test).
-include_lib("eunit/include/eunit.hrl").

blank_line_separator_returns_repeated_blank_spaces_and_line_for_width_test() ->
  ?assertEqual("     |     |     |\n", board_presenter:blank_line_separator(3)).

row_separator_returns_string_of_repeated_dashes_for_width_test() ->
  ?assertEqual("------------------\n", board_presenter:row_separator(3)).

value_row_string_returns_row_of_single_digit_numbers_with_separators_given_list_test() ->
  ?assertEqual("  1  |  2  |  3  |\n", board_presenter:value_row_string([1,2,3])).

value_row_string_returns_row_of_double_digit_numbers_with_separators_given_list_test() ->
  ?assertEqual(" 11  | 12  | 13  |\n", board_presenter:value_row_string([11,12,13])).

value_block_string_returns_a_string_of_lines_separated_with_values_given_test() ->
  RowValueList = [1,5,10],
  Width = 3,
  ExpectedString = "     |     |     |\n" ++
  "  1  |  5  | 10  |\n" ++
  "     |     |     |\n" ++
  "------------------\n",

  ?assertEqual(ExpectedString, board_presenter:value_block_string(Width, RowValueList)).

board_string_returns_string_of_given_board_test() ->
  Board = board:new_board(2),
  ExpectedBoardString = "     |     |\n" ++
  "  1  |  2  |\n" ++
  "     |     |\n" ++
  "------------\n" ++
  "     |     |\n" ++
  "  3  |  4  |\n" ++
  "     |     |\n" ++
  "------------\n",

  ?assertEqual(ExpectedBoardString, board_presenter:board_string(Board)).
