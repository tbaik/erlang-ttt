-module(board_test).
-include_lib("eunit/include/eunit.hrl").

board_of_1_returns_a_list_size_of_1_test() ->
  ?assertEqual(1, length(board:new_board(1))).

board_of_3_returns_a_list_size_of_9_test() ->
  ?assertEqual(9, length(board:new_board(3))).

board_of_0_returns_a_list_size_of_0_test() ->
  ?assertEqual(0, length(board:new_board(0))).

board_of_atom_returns_a_empty_list_test() ->
  ?assertEqual([], board:new_board(some_atom)).

place_piece_on_space_2_returns_board_with_updated_space_test() ->
  Board = board:new_board(2),
  ?assertEqual([1, "O", 3, 4], board:place_piece("O", "2", Board)).

place_piece_with_integer_move_test() ->
  Board = board:new_board(2),
  ?assertEqual([1, "O", 3, 4], board:place_piece("O", 2, Board)).

width_returns_the_square_root_of_list_test() ->
  Board = board:new_board(3),
  ?assertEqual(3, board:width(Board)).

valid_moves_returns_all_integers_in_list_test() ->
  Board = ["Hi", 2, 3, 4],
  ?assertEqual([2,3,4], board:valid_moves(Board)).

is_full_returns_true_if_no_valid_moves_test() ->
  Board = ["X", "O", "X", "O"],
  ?assert(board:is_full(Board)).

is_full_returns_false_if_any_valid_moves_test() ->
  Board = ["X", "O", "X", 4],
  ?assertNot(board:is_full(Board)).
