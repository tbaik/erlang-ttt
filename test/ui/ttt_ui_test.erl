-module(ttt_ui_test).
-include_lib("eunit/include/eunit.hrl").
-include("src/macros/console_ttt.hrl").

all_print_methods_call_on_io_print_message_with_message_test_() ->
  {foreach,
   fun setup_print_message/0,
   fun teardown/1,
   [fun() -> ?assertEqual(print_message_called, ttt_ui:print_board([1,2,3,4])) end,
    fun() -> ?assertEqual(print_message_called, ttt_ui:print_invalid_input_error()) end,
    fun() -> ?assertEqual(print_message_called, ttt_ui:print_invalid_move_error()) end,
   fun() -> ?assertEqual(print_message_called, ttt_ui:print_placed_piece("spot","piece","name")) end,
   fun() -> ?assertEqual(print_message_called, ttt_ui:print_tie_game()) end,
   fun() -> ?assertEqual(print_message_called, ttt_ui:print_winner("name")) end]}.

all_prompt_methods_call_on_io_prompt_with_message_test_() ->
  {foreach,
   fun setup_prompt/0,
   fun teardown/1,
   [fun() -> ?assertEqual(prompt_called, ttt_ui:prompt_turn()) end,
    fun() -> ?assertEqual(prompt_called, ttt_ui:prompt_human_or_ai()) end,
    fun() -> ?assertEqual(prompt_called, ttt_ui:prompt_play_again()) end,
    fun() -> ?assertEqual(prompt_called, ttt_ui:prompt_new_move("Player Name")) end]}.

setup_print_message() ->
  meck:new(?IO, [unstick]),
  meck:expect(?IO, print_message, fun(_Message) -> print_message_called end).

setup_prompt() ->
  meck:new(?IO, [unstick]),
  meck:expect(?IO, prompt, fun(_Message) -> prompt_called end).

teardown(_) ->
  meck:unload(?IO).
