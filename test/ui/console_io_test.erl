-module(console_io_test).
-include_lib("eunit/include/eunit.hrl").

print_message_outputs_exact_message_to_standard_out_test() ->
  meck:new(io, [unstick]),
  meck:expect(io, format, fun(Message) -> Message end),
  ?assertEqual("Michael Scott", console_io:print_message("Michael Scott")),
  meck:unload(io).

prompt_grabs_input_without_new_line_test() ->
  meck:new(io, [unstick]),
  meck:expect(io, get_line, fun(_Message) -> "Michael Scott\n" end),
  ?assertEqual("Michael Scott", console_io:prompt("What is your name?\n")),
  meck:unload(io).
