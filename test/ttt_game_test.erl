-module(ttt_game_test).
-include_lib("eunit/include/eunit.hrl").
-include("src/macros/console_ttt.hrl").

get_input_calls_choose_move_on_current_player_test() ->
  CurrentPlayerModule = some_player_module,
  GameState = dict:store(players, [[CurrentPlayerModule, "X", "Player 1"], []], dict:new()),

  meck:new(CurrentPlayerModule, [non_strict]),
  meck:expect(CurrentPlayerModule, choose_move, fun(_GameState) -> ok end),

  ttt_game:get_input(GameState),
  ?assert(meck:called(CurrentPlayerModule, choose_move, [GameState])),
  meck:unload(CurrentPlayerModule).

update_returns_new_state_with_made_move_and_players_switched_if_valid_test() ->
  Player1 = [player_module, "O", "Player 1"],
  Player2 = [player_module, "X", "Player 2"],

  GameState =
  ?GAME_STATE:update_multiple(dict:new(),
                              [game_board, players],
                              [[1,2,3,4], [Player1, Player2]]),

  ExpectedNewGameState =
  ?GAME_STATE:update_multiple(GameState,
                              [game_board, players, update_status, last_move, is_running],
                              [[1, "O", 3, 4], [Player2, Player1], made_move, "2", true]),
  ?assertEqual(ExpectedNewGameState, ttt_game:update(GameState, "2")).

update_returns_update_status_as_game_over_and_is_running_as_false_if_game_is_over_test() ->
  Player1 = [player_module, "O", "Player 1"],
  Player2 = [player_module, "X", "Player 2"],

  GameState =
  ?GAME_STATE:update_multiple(dict:new(),
                              [game_board, players],
                              [["O",2, "X", "X"], [Player1, Player2]]),

  ExpectedNewGameState =
  ?GAME_STATE:update_multiple(GameState,
                              [game_board, players, update_status, last_move, is_running],
                              [["O", "O", "X", "X"], [Player2, Player1], game_over, "2", false]),
  ?assertEqual(ExpectedNewGameState, ttt_game:update(GameState, "2")).

update_returns_same_game_state_with_invalid_move_status_if_invalid_test() ->
  Board = [1, 2, "O", 4],
  Players = [[player_module, "O", "Name"], []],
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, players, update_status],
                                          [Board, Players, invalid_move]),
  InputMove = "invalid_move",

  ?assertEqual(GameState, ttt_game:update(GameState, InputMove)).

run_all_draw_test_() ->
  {foreach,
   fun setup_draw/0,
   fun teardown_draw/1,
   [fun draw_prints_move_placed_and_board_when_made_move_/0,
    fun draw_prints_invalid_move_and_board_when_status_is_invalid_move_/0,
    fun draw_prints_tie_game_when_game_over_and_no_winner_/0,
    fun draw_prints_winner_when_game_over_and_has_winner_/0]}.

setup_draw() ->
  meck:new(?UI),
  meck:expect(?UI, print_placed_piece, fun(_Spot, _Piece, _Name) -> ok end),
  meck:expect(?UI, print_board, fun(_Board) -> ok end),
  meck:expect(?UI, print_invalid_move_error, fun() -> ok end),
  meck:expect(?UI, print_tie_game, fun() -> ok end),
  meck:expect(?UI, print_winner, fun(_Name) -> ok end).

teardown_draw(_) ->
  meck:unload(?UI).

draw_prints_move_placed_and_board_when_made_move_() ->
  Board = [1, 2, "O", 4],
  Players = [[], [player_module, "O", "Name"]],
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, players, update_status, last_move],
                                          [Board, Players, made_move, "3"]),

  ttt_game:draw(GameState),
  ?assert(meck:called(?UI, print_placed_piece, ["3", "O", "Name"])),
  ?assert(meck:called(?UI, print_board, [Board])).

draw_prints_invalid_move_and_board_when_status_is_invalid_move_() ->
  Board = [1, 2, 3, 4],
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, update_status],
                                          [Board, invalid_move]),

  ttt_game:draw(GameState),
  ?assert(meck:called(?UI, print_invalid_move_error, [])),
  ?assert(meck:called(?UI, print_board, [Board])).

draw_prints_tie_game_when_game_over_and_no_winner_() ->
  Board = [1, 2, "O", 4],
  Players = [[human_player, "X", "Player 1"], [human_player, "O", "Player 2"]],
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, update_status, players, last_move],
                                          [Board, game_over, Players, "3"]),

  ttt_game:draw(GameState),
  ?assert(meck:called(?UI, print_tie_game, [])).

draw_prints_winner_when_game_over_and_has_winner_() ->
  Board = ["X", "X", "X", 4, 5, 6, 7, 8, 9],
  Players = [[human_player, "X", "Player 1"], [human_player, "O", "Player 2"]],
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, update_status, players, last_move],
                                          [Board, game_over, Players, "3"]),

  ttt_game:draw(GameState),
  ?assert(meck:called(?UI, print_winner, ["Player 2"])).
