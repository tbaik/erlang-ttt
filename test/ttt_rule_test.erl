-module(ttt_rule_test).
-include_lib("eunit/include/eunit.hrl").
-include("src/macros/console_ttt.hrl").

is_valid_move_returns_true_if_space_is_not_taken_test() ->
  Board = [1,2,3,4,5,6,7,8,9],
  ?assert(ttt_rule:is_valid_move(Board, "1")).

is_valid_move_returns_false_if_space_is_taken_test() ->
  Board = ["blah",2,3,4,5,6,7,8,9],
  ?assertNot(ttt_rule:is_valid_move(Board, "1")).

is_valid_move_returns_false_if_move_is_invalid_input_test() ->
  Board = [1,2,3,4,5,6,7,8,9],
  ?assertNot(ttt_rule:is_valid_move(Board, "not_int")).

is_valid_move_returns_false_if_move_is_out_of_index_test() ->
  Board = [1,2,3,4,5,6,7,8,9],
  ?assertNot(ttt_rule:is_valid_move(Board, "9001")).

determine_winner_returns_tie_if_no_winner_test() ->
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, players],
                                          [["X", "O", 3, 4, 5, 6, 7, 8, 9], []]),
  ?assertEqual(tie, ttt_rule:determine_winner(GameState)).

determine_winner_returns_player_name_of_winner_if_has_winner_test() ->
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [game_board, players],
                                          [["X", "X", "X", 4, 5, 6, 7, 8, 9],
                                           [[human_player, "O", "Player 1"],
                                            [human_player, "X", "Player 2"]]]),
  ?assertEqual("Player 2", ttt_rule:determine_winner(GameState)).

game_over_returns_true_when_board_is_full_test() ->
  Board = ["X", "O", "X", "O"],
  ?assert(ttt_rule:game_over(Board)).

game_over_returns_true_when_board_has_winner_test() ->
  Board = ["X", "X", "X", 4, 5, 6, 7, 8, 9],
  ?assert(ttt_rule:game_over(Board)).

game_over_returns_false_when_board_is_not_full_and_no_winner_test() ->
  Board = ["X", "O", "X", 4, 5, 6, 7, 8, 9],
  ?assertNot(ttt_rule:game_over(Board)).

has_winner_returns_true_when_diagonal_winner_test() ->
  Board = ["O",2,3,4,"O",6,7,8,"O"],
  ?assert(ttt_rule:has_winner(Board)).

has_winner_returns_true_when_vertical_winner_test() ->
  Board = ["O",2,3,"O",5,6,"O",8,9],
  ?assert(ttt_rule:has_winner(Board)).

has_winner_returns_true_when_horizontal_winner_test() ->
  Board = ["O","O","O",4,5,6,7,8,9],
  ?assert(ttt_rule:has_winner(Board)).

any_true_returns_true_when_at_least_one_is_true_in_list_test() ->
  List = [true, false, false],
  ?assert(ttt_rule:any_true(List)).

any_true_returns_false_when_none_are_true_in_list_test() ->
  List = [false, false, false],
  ?assertNot(ttt_rule:any_true(List)).

has_winner_from_separation_returns_true_given_winning_separation_test() ->
  SeparatedList = [["O", "O", "O"], [4,5,6], [7,8,9]],
  ?assert(ttt_rule:has_winner_from_separation(SeparatedList)).

has_winner_from_separation_returns_false_given_losing_separation_test() ->
  SeparatedList = [["O", "O", "X"], [4,5,6], [7,8,9]],
  ?assertNot(ttt_rule:has_winner_from_separation(SeparatedList)).

contains_same_pieces_returns_true_when_all_items_are_equal_in_list_test() ->
  List = ["Hi", "Hi", "Hi"],
  ?assert(ttt_rule:contains_same_pieces(List)).

contains_same_pieces_returns_false_when_all_items_are_not_equal_in_list_test() ->
  List = ["Hi", "Hi", "Hello"],
  ?assertNot(ttt_rule:contains_same_pieces(List)).

has_horizontal_winner_returns_true_given_horizontal_winning_board_test() ->
  Board = ["O","O","O",4,5,6,7,8,9],
  Width = 3,
  ?assert(ttt_rule:has_horizontal_winner(Board, Width)).

has_horizontal_winner_returns_false_given_non_winning_board_test() ->
  Board = ["O","X","O",4,5,6,7,8,9],
  Width = 3,
  ?assertNot(ttt_rule:has_horizontal_winner(Board, Width)).

has_vertical_winner_returns_true_given_vertical_winning_board_test() ->
  Board = ["O",2,3,"O",5,6,"O",8,9],
  Width = 3,
  ?assert(ttt_rule:has_vertical_winner(Board, Width)).

has_vertical_winner_returns_false_given_non_winning_board_test() ->
  Board = ["O","X","O",4,5,6,7,8,9],
  Width = 3,
  ?assertNot(ttt_rule:has_vertical_winner(Board, Width)).

has_diagonal_winner_returns_true_given_diagonal_winning_board_test() ->
  Board = ["O",2,3,4,"O",6,7,8,"O"],
  Width = 3,
  ?assert(ttt_rule:has_diagonal_winner(Board, Width)).

has_diagonal_winner_returns_false_given_non_winning_board_test() ->
  Board = ["O","X","O",4,5,6,7,8,9],
  Width = 3,
  ?assertNot(ttt_rule:has_diagonal_winner(Board, Width)).
