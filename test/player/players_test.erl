-module(players_test).
-include_lib("eunit/include/eunit.hrl").

name_returns_player_name_given_player_list_test() ->
  Player = [player_module, "O", "Player Name"],
  ?assertEqual("Player Name", players:name(Player)).

module_returns_player_module_given_player_list_test() ->
  Player = [player_module, "O", "Player Name"],
  ?assertEqual(player_module, players:module(Player)).

piece_returns_player_piece_given_player_list_test() ->
  Player = [player_module, "O", "Player Name"],
  ?assertEqual("O", players:piece(Player)).

current_player_returns_first_player_in_players_list_test() ->
  Player1 = [player_module, "X", "Player 1"],
  Player2 = [player_module, "O", "Player 2"],
  Players = [Player1, Player2],
  ?assertEqual(Player1, players:current_player(Players)).

current_player_piece_returns_correctly_given_players_list_test() ->
  Players = [[player_module, "X", "Player 1"], []],
  ?assertEqual("X", players:current_player_piece(Players)).

previous_player_returns_second_player_in_players_list_test() ->
  Player1 = [player_module, "X", "Player 1"],
  Player2 = [player_module, "O", "Player 2"],
  Players = [Player1, Player2],
  ?assertEqual(Player2, players:previous_player(Players)).

change_turn_returns_reversed_players_test() ->
  Player1 = [player_module, "X", "Player 1"],
  Player2 = [player_module, "O", "Player 2"],
  Players = [Player1, Player2],
  ?assertEqual([Player2, Player1], players:change_turn(Players)).

is_computer_turn_returns_true_if_current_player_is_computer_test() ->
  Player1 = [hard_ai, "X", "Computer"],
  Player2 = [player_module, "O", "Player 2"],
  Players = [Player1, Player2],
  ?assert(players:is_computer_turn(Players)).

is_computer_turn_returns_false_if_current_player_is_not_computer_test() ->
  Player1 = [hard_ai, "X", "Computer"],
  Player2 = [player_module, "O", "Player 2"],
  Players = [Player2, Player1],
  ?assertNot(players:is_computer_turn(Players)).
