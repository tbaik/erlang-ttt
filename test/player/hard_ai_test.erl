-module(hard_ai_test).
-include_lib("eunit/include/eunit.hrl").

choose_move_returns_the_index_of_maximum_score_test() ->
  Board = ["X", 2, 3, 4, 5, 6, 7, 8, 9],
  Players = [[hard_ai, "O", "Computer"], [human_player, "X", "Player 1"]],
  GS = dict:store(game_board, Board, dict:new()),
  GameState = dict:store(players, Players, GS),
  ?assertEqual("5", hard_ai:choose_move(GameState)).

minimax_returns_1_if_full_with_out_winner_test() ->
  Players = [[human_player, "X", "Player 1"], [hard_ai, "O", "Computer"]],
  Board = ["X", "O", "X", "O", "O", "X", "X", "X", "O"],
  ?assertEqual(1, hard_ai:minimax(Players, Board)).

minimax_returns_2_if_has_winner_with_current_player_human_test() ->
  Players = [[human_player, "X", "Player 1"], [hard_ai, "O", "Computer"]],
  Board = ["X", "X", "X", "O", "O", 6, 7, 8, 9],
  ?assertEqual(2, hard_ai:minimax(Players, Board)).

minimax_returns_0_if_has_winner_with_current_player_computer_test() ->
  Players = [[hard_ai, "O", "Computer"], [human_player, "X", "Player 1"]],
  Board = ["O", "O", "O", "X", "X", 6, 7, 8, 9],
  ?assertEqual(0, hard_ai:minimax(Players, Board)).

minimax_returns_eval_of_2_for_a_game_bound_to_win_test() ->
  Players = [[hard_ai, "O", "Computer"], [human_player, "X", "Player 1"]],
  Board = [1, "X", 3, 4, "O", 6, 7, "X", 9],
  ?assertEqual(2, hard_ai:minimax(Players, Board)).

minimax_returns_eval_of_1_for_a_game_for_maximum_tie_game_test() ->
  Players = [[hard_ai, "O", "Computer"], [human_player, "X", "Player 1"]],
  Board = ["X", 2, 3, 4, 5, 6, 7, 8, 9],
  ?assertEqual(1, hard_ai:minimax(Players, Board)).

minimax_returns_eval_of_0_for_a_game_bound_to_lose_test() ->
  Players = [[hard_ai, "O", "Computer"], [human_player, "X", "Player 1"]],
  Board = ["X", "O", 3, 4, "X", 6, 7, 8, 9],
  ?assertEqual(0, hard_ai:minimax(Players, Board)).

get_scores_returns_list_of_score_evals_from_each_valid_move_test() ->
  Players = [[hard_ai, "O", "Computer"], [human_player, "X", "Player 1"]],
  Board = ["O", 2, "X", 4, 5, 6, 7, 8, 9],
  ExpectedEvalScores = [0, 2, 1, 1, 2, 1, 2],
  ?assertEqual(ExpectedEvalScores, hard_ai:get_scores(Players, Board)).
