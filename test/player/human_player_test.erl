-module(human_player_test).
-include_lib("eunit/include/eunit.hrl").
-include("src/macros/console_ttt.hrl").

choose_move_returns_user_input_move_and_calls_prompt_new_move_and_print_board_test() ->
  meck:new(?UI),
  meck:expect(?UI, prompt_new_move, fun(_PlayerName) -> "2"  end),
  meck:expect(?UI, print_board, fun(_Board) -> ok  end),
  GameState = ?GAME_STATE:update_multiple(dict:new(),
                                          [players, game_board],
                                          [[[human_player, "O", "Player Name"],[]], [1,2,3,4]]),

  ?assertEqual("2", human_player:choose_move(GameState)),
  ?assert(meck:called(?UI, prompt_new_move, ["Player Name"])),
  ?assert(meck:called(?UI, print_board, [[1,2,3,4]])),
  meck:unload(?UI).
