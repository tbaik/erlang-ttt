-module(ttt_game_state_test).
-include_lib("eunit/include/eunit.hrl").

get_players_returns_players_from_dict_test() ->
  GameState = dict:store(players, "Some players", dict:new()),
  ?assertEqual("Some players", ttt_game_state:get_players(GameState)).

get_board_returns_game_board_from_dict_test() ->
  GameState = dict:store(game_board, [1,2], dict:new()),
  ?assertEqual([1,2], ttt_game_state:get_board(GameState)).

get_update_status_returns_status_atom_from_dict_test() ->
  GameState = dict:store(update_status, made_move, dict:new()),
  ?assertEqual(made_move, ttt_game_state:get_update_status(GameState)).

get_last_move_returns_last_input_from_dict_test() ->
  GameState = dict:store(last_move, "2", dict:new()),
  ?assertEqual("2", ttt_game_state:get_last_move(GameState)).

get_is_running_returns_correct_boolean_from_dict_test() ->
  GameState = dict:store(is_running, true, dict:new()),
  ?assertEqual(true, ttt_game_state:get_is_running(GameState)).

update_returns_updated_game_state_test() ->
  ExpectedGameState = dict:store(key, value, dict:new()),
  ?assertEqual(ExpectedGameState, ttt_game_state:update(dict:new(), key, value)).

update_multiple_takes_list_of_keys_and_values_and_updates_1_test() ->
  ExpectedGameState = dict:store(key, value, dict:new()),
  ?assertEqual(ExpectedGameState, ttt_game_state:update_multiple(dict:new(), [key], [value])).

update_multiple_takes_list_of_keys_and_values_and_updates_2_test() ->
  GS = dict:store(key, value, dict:new()),
  ExpectedGameState = dict:store(key2, value2, GS),
  ?assertEqual(ExpectedGameState, ttt_game_state:update_multiple(dict:new(), [key, key2], [value, value2])).

