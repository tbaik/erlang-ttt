-module(matrix_test).
-include_lib("eunit/include/eunit.hrl").

separate_into_rows_returns_partitions_of_list_into_given_width_test() ->
  List = [1,2,3,4,5,6,7,8,9],
  ?assertEqual([[1,2,3],[4,5,6],[7,8,9]], matrix:create_matrix(List, 3)).

separate_into_columns_returns_columns_given_width_test() ->
  List = [1,2,3,4,5,6,7,8,9],
  ?assertEqual([[1,4,7],[2,5,8],[3,6,9]], matrix:separate_into_columns(List, 3)).

separate_into_diagonals_returns_both_diagonals_given_width_test() ->
  List = [1,2,3,4,5,6,7,8,9],
  ?assertEqual([[1,5,9],[3,5,7]], matrix:separate_into_diagonals(List, 3)).
