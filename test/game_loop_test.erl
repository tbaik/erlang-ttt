-module(game_loop_test).
-include_lib("eunit/include/eunit.hrl").
-include("src/macros/console_ttt.hrl").

-define(GAME_MODULE, some_game).
-define(INPUT, some_input).

run_all_game_loop_test_() ->
  {foreach,
   fun setup/0,
   fun teardown/1,
   [fun run_calls_get_input_on_game_module_when_running_/0,
    fun run_calls_update_on_game_module_with_game_state_when_running_/0,
    fun run_calls_draw_on_game_module_with_new_game_state_when_running_/0,
    fun run_returns_finished_game_when_not_running_/0]}.

setup() ->
  FinishedGameState = ?GAME_STATE:update(dict:new(), is_running, false),
  meck:new(?GAME_MODULE, [non_strict]),
  meck:expect(?GAME_MODULE, get_input, fun(_GameState) -> ?INPUT end),
  meck:expect(?GAME_MODULE, update, fun(_GameState, _Input) -> FinishedGameState end),
  meck:expect(?GAME_MODULE, draw, fun(_GameState) -> ok end).

teardown(_) ->
  meck:unload(?GAME_MODULE).

run_calls_get_input_on_game_module_when_running_() ->
  GameState = ?GAME_STATE:update(dict:new(), is_running, true),
  game_loop:run(?GAME_MODULE, GameState),
  ?assert(meck:called(?GAME_MODULE, get_input, [GameState])).

run_calls_update_on_game_module_with_game_state_when_running_() ->
  GameState = ?GAME_STATE:update(dict:new(), is_running, true),
  game_loop:run(?GAME_MODULE, GameState),
  ?assert(meck:called(?GAME_MODULE, update, [GameState, ?INPUT])).

run_calls_draw_on_game_module_with_new_game_state_when_running_() ->
  GameState = ?GAME_STATE:update(dict:new(), is_running, true),
  NewGameState = ?GAME_STATE:update(dict:new(), is_running, false),

  game_loop:run(?GAME_MODULE, GameState),
  ?assert(meck:called(?GAME_MODULE, draw, [NewGameState])).

run_returns_finished_game_when_not_running_() ->
  GameState = ?GAME_STATE:update(dict:new(), is_running, false),
  ?assertEqual(finished_game, game_loop:run(some_game, GameState)).
