-module(ttt_setup_test).
-include_lib("eunit/include/eunit.hrl").
-include("src/macros/players.hrl").
-include("src/macros/console_ttt.hrl").

run_all_ttt_setup_test_() ->
  {foreach,
   fun setup/0,
   fun teardown/1,
   [fun build_initial_game_state_returns_players_from_given_input_/0,
    fun build_initial_game_state_returns_board_of_default_width_3_/0,
    fun build_turn_returns_X_when_given_1_/0,
    fun build_turn_returns_O_when_given_2_/0,
    fun build_turn_prints_error_and_calls_build_turn_again_on_wrong_input_/0,
    fun build_opponent_returns_computer_when_given_1_/0,
    fun build_opponent_returns_player_2_when_given_2_/0,
    fun build_opponent_prints_error_and_calls_build_opponent_again_on_wrong_input_/0,
    fun build_players_returns_hard_ai_module_first_given_computer_and_O_/0,
    fun build_players_returns_human_player_module_first_given_computer_and_X_/0,
    fun build_players_returns_human_player_1_then_player_2_given_player_2_and_X_/0,
    fun build_players_returns_human_player_1_then_player_2_given_player_2_and_O_/0]}.

setup() ->
  meck:new(?UI).

teardown(_) ->
  meck:unload(?UI).

build_initial_game_state_returns_players_from_given_input_() ->
  meck:expect(?UI, prompt_turn, fun() -> "1" end),
  meck:expect(?UI, prompt_human_or_ai, fun() -> "2" end),
  Players = dict:fetch(players, ttt_setup:build_initial_game_state()),
  ?assertEqual([[human_player, ?X_PIECE, ?PLAYER_1_NAME],[human_player, ?O_PIECE, ?PLAYER_2_NAME]], Players).

build_initial_game_state_returns_board_of_default_width_3_() ->
  meck:expect(?UI, prompt_turn, fun() -> "1" end),
  meck:expect(?UI, prompt_human_or_ai, fun() -> "2" end),
  Board = dict:fetch(game_board, ttt_setup:build_initial_game_state()),
  ?assertEqual(board:new_board(3), Board).

build_turn_returns_X_when_given_1_() ->
  meck:expect(?UI, prompt_turn, fun() -> "1" end),
  ?assertEqual(?X_PIECE, ttt_setup:build_turn()).

build_turn_returns_O_when_given_2_() ->
  meck:expect(?UI, prompt_turn, fun() -> "2" end),
  ?assertEqual(?O_PIECE, ttt_setup:build_turn()).

build_turn_prints_error_and_calls_build_turn_again_on_wrong_input_() ->
  meck:expect(?UI, print_invalid_input_error, fun() -> ok end),
  meck:sequence(?UI, prompt_turn, 0, ["invalid_input", "2"]),
  ttt_setup:build_turn(),
  ?assert(meck:called(?UI, print_invalid_input_error, [])).

build_opponent_returns_computer_when_given_1_() ->
  meck:expect(?UI, prompt_human_or_ai, fun() -> "1" end),
  ?assertEqual(?COMPUTER_NAME, ttt_setup:build_opponent()).

build_opponent_returns_player_2_when_given_2_() ->
  meck:expect(?UI, prompt_human_or_ai, fun() -> "2" end),
  ?assertEqual(?PLAYER_2_NAME, ttt_setup:build_opponent()).

build_opponent_prints_error_and_calls_build_opponent_again_on_wrong_input_() ->
  meck:expect(?UI, print_invalid_input_error, fun() -> ok end),
  meck:sequence(?UI, prompt_human_or_ai, 0, ["invalid_input", "2"]),
  ttt_setup:build_opponent(),
  ?assert(meck:called(?UI, print_invalid_input_error, [])).

build_players_returns_hard_ai_module_first_given_computer_and_O_() ->
  meck:expect(?UI, prompt_turn, fun() -> "2" end),
  meck:expect(?UI, prompt_human_or_ai, fun() -> "1" end),
  ?assertEqual([[hard_ai, ?X_PIECE, ?COMPUTER_NAME],[human_player, ?O_PIECE, ?PLAYER_1_NAME]],
               ttt_setup:build_players()).

build_players_returns_human_player_module_first_given_computer_and_X_() ->
  meck:expect(?UI, prompt_turn, fun() -> "1" end),
  meck:expect(?UI, prompt_human_or_ai, fun() -> "1" end),
  ?assertEqual([[human_player, ?X_PIECE, ?PLAYER_1_NAME],[hard_ai, ?O_PIECE, ?COMPUTER_NAME]],
               ttt_setup:build_players()).

build_players_returns_human_player_1_then_player_2_given_player_2_and_X_() ->
  meck:expect(?UI, prompt_turn, fun() -> "1" end),
  meck:expect(?UI, prompt_human_or_ai, fun() -> "2" end),
  ?assertEqual([[human_player, ?X_PIECE, ?PLAYER_1_NAME],[human_player, ?O_PIECE, ?PLAYER_2_NAME]],
               ttt_setup:build_players()).

build_players_returns_human_player_1_then_player_2_given_player_2_and_O_() ->
  meck:expect(?UI, prompt_turn, fun() -> "2" end),
  meck:expect(?UI, prompt_human_or_ai, fun() -> "2" end),
  ?assertEqual([[human_player, ?X_PIECE, ?PLAYER_2_NAME],[human_player, ?O_PIECE, ?PLAYER_1_NAME]],
               ttt_setup:build_players()).
