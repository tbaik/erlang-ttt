Erlang-TTT
=====

A game of Tic-tac-toe written in Erlang.

### Features:

- 3x3 Game Board
- Play against an unbeatable computer
- Play against another person

### Compile the project
```
./rebar get-deps (only have to run once)
./rebar compile
```

### To start the game:
```
escript run_ttt
```

### To run eunit tests:
```
./rebar eunit
```

### Erlang Installation
Please see the [Erlang Installation Guide](http://www.erlang.org/doc/installation_guide/INSTALL.html) for information on how to install erlang.

For Mac users: You can download pre-compiled packages [here!](http://rudix.org/packages/erlang.html)
